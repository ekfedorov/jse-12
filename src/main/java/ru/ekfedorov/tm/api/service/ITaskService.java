package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task removeOneById(String id);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task updateTaskById(String id, String name, String description);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task finishTaskById(String id);

    Task finishTaskByIndex(Integer index);

    Task finishTaskByName(String name);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusByName(String name, Status status);

}
