package ru.ekfedorov.tm.api.controller;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void showProjectByIndex();

    void showProjectByName();

    void showProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void removeProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void startProjectById();

    void startProjectByIndex();

    void startProjectByName();

    void finishProjectById();

    void finishProjectByIndex();

    void finishProjectByName();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void changeProjectStatusByName();

}
