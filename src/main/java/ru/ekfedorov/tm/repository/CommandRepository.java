package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.ICommandRepository;
import ru.ekfedorov.tm.constant.ArgumentConst;
import ru.ekfedorov.tm.constant.TerminalConst;
import ru.ekfedorov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Display developer info."
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Display list of terminal commands."
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Display program version."
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null , "Close application."
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO , "Show system info."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null , "Show program commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null , "Show program arguments."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.CMD_TASK_CREATE, null , "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.CMD_TASK_CLEAR, null , "Clear all task."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.CMD_TASK_LIST, null , "Show all task."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.CMD_PROJECT_CREATE, null , "Create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.CMD_PROJECT_CLEAR, null , "Clear all project."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.CMD_PROJECT_LIST, null , "Show all project."
    );

    public static final Command SHOW_TASK_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_VIEW_BY_INDEX, null, "Show task by index."
    );

    public static final Command SHOW_TASK_BY_NAME = new Command(
            TerminalConst.CMD_TASK_VIEW_BY_NAME, null, "Show task by name."
    );

    public static final Command SHOW_TASK_BY_ID = new Command(
            TerminalConst.CMD_TASK_VIEW_BY_ID, null, "Show task by id."
    );


    public static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_UPDATE_BY_INDEX, null, "Update task by index."
    );

    public static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_TASK_UPDATE_BY_ID, null, "Update task by id."
    );

    public static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_INDEX, null, "Remove task by index."
    );

    public static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_ID, null, "Remove task by id."
    );

    public static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_NAME, null, "Remove task by name."
    );

    public static final Command SHOW_PROJECT_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_VIEW_BY_INDEX, null, "Show project by index."
    );

    public static final Command SHOW_PROJECT_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_VIEW_BY_NAME, null, "Show project by name."
    );

    public static final Command SHOW_PROJECT_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_VIEW_BY_ID, null, "Show project by id."
    );

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX, null, "Update project by index."
    );

    public static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_UPDATE_BY_ID, null, "Update project by id."
    );

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX, null, "Remove project by index."
    );

    public static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_ID, null, "Remove project by id."
    );

    public static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_NAME, null, "Remove project by name."
    );

    public static final Command START_PROJECT_BY_ID = new Command(
            TerminalConst.CMD_START_PROJECT_BY_ID, null, "Start project by id."
    );

    public static final Command START_PROJECT_BY_INDEX = new Command(
            TerminalConst.CMD_START_PROJECT_BY_INDEX, null, "Start project by index."
    );

    public static final Command START_PROJECT_BY_NAME = new Command(
            TerminalConst.CMD_START_PROJECT_BY_NAME, null, "Start project by name."
    );

    public static final Command FINISH_PROJECT_BY_ID = new Command(
            TerminalConst.CMD_FINISH_PROJECT_BY_ID, null, "Finish project by id."
    );

    public static final Command FINISH_PROJECT_BY_INDEX = new Command(
            TerminalConst.CMD_FINISH_PROJECT_BY_INDEX, null, "Finish project by index."
    );

    public static final Command FINISH_PROJECT_BY_NAME = new Command(
            TerminalConst.CMD_FINISH_PROJECT_BY_NAME, null, "Finish project by name."
    );

    public static final Command CHANGE_PROJECT_STATUS_BY_ID = new Command(
            TerminalConst.CMD_CHANGE_PROJECT_STATUS_BY_ID, null, "Change project status by id."
    );

    public static final Command CHANGE_PROJECT_STATUS_BY_INDEX = new Command(
            TerminalConst.CMD_CHANGE_PROJECT_STATUS_BY_INDEX, null, "Change project status by index."
    );

    public static final Command CHANGE_PROJECT_STATUS_BY_NAME = new Command(
            TerminalConst.CMD_CHANGE_PROJECT_STATUS_BY_NAME, null, "Change project status by name."
    );

    public static final Command START_TASK_BY_ID = new Command(
            TerminalConst.CMD_START_TASK_BY_ID, null, "Start task by id."
    );

    public static final Command START_TASK_BY_INDEX = new Command(
            TerminalConst.CMD_START_TASK_BY_INDEX, null, "Start task by index."
    );

    public static final Command START_TASK_BY_NAME = new Command(
            TerminalConst.CMD_START_TASK_BY_NAME, null, "Start task by name."
    );

    public static final Command FINISH_TASK_BY_ID = new Command(
            TerminalConst.CMD_FINISH_TASK_BY_ID, null, "Finish task by id."
    );

    public static final Command FINISH_TASK_BY_INDEX = new Command(
            TerminalConst.CMD_FINISH_TASK_BY_INDEX, null, "Finish task by index."
    );

    public static final Command FINISH_TASK_BY_NAME = new Command(
            TerminalConst.CMD_FINISH_TASK_BY_NAME, null, "Finish task by name."
    );

    public static final Command CHANGE_TASK_STATUS_BY_ID = new Command(
            TerminalConst.CMD_CHANGE_TASK_STATUS_BY_ID, null, "Change task status by id."
    );

    public static final Command CHANGE_TASK_STATUS_BY_INDEX = new Command(
            TerminalConst.CMD_CHANGE_TASK_STATUS_BY_INDEX, null, "Change task status by index."
    );

    public static final Command CHANGE_TASK_STATUS_BY_NAME = new Command(
            TerminalConst.CMD_CHANGE_TASK_STATUS_BY_NAME, null, "Change task status by name."
    );
    public static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, COMMANDS, ARGUMENTS,
            TASK_CREATE, TASK_CLEAR, TASK_LIST,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            SHOW_TASK_BY_INDEX, SHOW_TASK_BY_NAME, SHOW_TASK_BY_ID,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_NAME,
            SHOW_PROJECT_BY_INDEX, SHOW_PROJECT_BY_NAME, SHOW_PROJECT_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_NAME,
            START_PROJECT_BY_ID, START_PROJECT_BY_INDEX, START_PROJECT_BY_NAME,
            FINISH_PROJECT_BY_ID, FINISH_PROJECT_BY_INDEX, FINISH_PROJECT_BY_NAME,
            CHANGE_PROJECT_STATUS_BY_ID, CHANGE_PROJECT_STATUS_BY_INDEX, CHANGE_PROJECT_STATUS_BY_NAME,
            START_TASK_BY_ID, START_TASK_BY_INDEX, START_TASK_BY_NAME,
            FINISH_TASK_BY_ID, FINISH_TASK_BY_INDEX, FINISH_TASK_BY_NAME,
            CHANGE_TASK_STATUS_BY_ID, CHANGE_TASK_STATUS_BY_INDEX, CHANGE_TASK_STATUS_BY_NAME,
            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
